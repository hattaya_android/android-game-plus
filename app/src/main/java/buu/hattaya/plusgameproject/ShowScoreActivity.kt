package buu.hattaya.plusgameproject

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView

class ShowScoreActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_show_score)
        val amount:String = intent.getStringExtra("amount")?:"Unknow"
        val txtAmount = findViewById<TextView>(R.id.txtAmount)
        txtAmount.text = amount

        val correct:String = intent.getStringExtra("correct")?:"Unknow"
        val txtCorrect = findViewById<TextView>(R.id.txtCorrect)
        txtCorrect.text = correct

        val incorrect:String = intent.getStringExtra("incorrect")?:"Unknow"
        val txtIncorrect = findViewById<TextView>(R.id.txtIncorrect)
        txtIncorrect.text = incorrect
    }
}