package buu.hattaya.plusgameproject

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import kotlin.random.Random

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        timer()
        Game()
    }

    var correctScore: Int = 0
    var incorrectScore: Int = 0
    var numberOfClauses:Int = -1

    fun timer () {
        val timer = findViewById<TextView>(R.id.txtTimer)

        object : CountDownTimer(61000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                timer.setText("Time     " +millisUntilFinished / 1000)
            }
            override fun onFinish() {
                timer.setText("Time's finished !")
                Toast.makeText(this@MainActivity, "Game finished" , Toast.LENGTH_LONG).show()
                val intent = Intent(this@MainActivity, ShowScoreActivity:: class.java)
                intent.putExtra("amount", numberOfClauses.toString())
                intent.putExtra("correct", correctScore.toString())
                intent.putExtra("incorrect", incorrectScore.toString())
                startActivity(intent)
            }
        }.start()
    }

    fun Game() {
        numberOfClauses ++
        val number1 = findViewById<TextView>(R.id.txtNumber1)
        val numberRandom1 =  Random.nextInt(0,9)
        number1.text = numberRandom1.toString()

        val number2 = findViewById<TextView>(R.id.txtNumber2)
        val numberRandom2 =  Random.nextInt(0,9)
        number2.text = numberRandom2.toString()

        val amount = findViewById<TextView>(R.id.txtAmount)
        amount.text = numberOfClauses.toString()

        val answer: Int = numberRandom1 + numberRandom2

        val position = Random.nextInt(0,3)

        val choice1 = findViewById<Button>(R.id.btnChoice1)
        val choice2 = findViewById<Button>(R.id.btnChoice2)
        val choice3 = findViewById<Button>(R.id.btnChoice3)

        if (position == 1){
            choice1.text = (answer+1).toString()
            choice2.text = (answer-1).toString()
            choice3.text = answer.toString()
        } else if (position == 2) {
            choice1.text = (answer-1).toString()
            choice2.text = answer.toString()
            choice3.text = (answer+1).toString()
        } else {
            choice1.text = answer.toString()
            choice2.text = (answer-1).toString()
            choice3.text = (answer+1).toString()
        }

        val scoreCorrect = findViewById<TextView>(R.id.txtpointRight)
        scoreCorrect.text = correctScore.toString()
        val scoreIncorrect = findViewById<TextView>(R.id.txtpointWrong)
        scoreIncorrect.text = incorrectScore.toString()

        val declare = findViewById<TextView>(R.id.txtDeclare)

        fun addCorrect(score: Int) {
            correctScore ++
            scoreCorrect.text = correctScore.toString()
            declare.text = " Correct !!"
        }
        fun addIncorrect(score: Int) {
            incorrectScore ++
            scoreIncorrect.text = incorrectScore.toString()
            declare.text = " Incorrect !!"
        }

        choice1.setOnClickListener {
            if (choice1.text.toString() == answer.toString()) {
                addCorrect(correctScore)
                Game()
            } else {
                addIncorrect(incorrectScore)
                Game()
            }
        }
        choice2.setOnClickListener {
            if (choice2.text.toString() == answer.toString()) {
                addCorrect(correctScore)
                Game()
            } else {
                addIncorrect(incorrectScore)
                Game()
            }
        }
        choice3.setOnClickListener {
            if (choice3.text.toString() == answer.toString()) {
                addCorrect(correctScore)
                Game()
            } else {
                addIncorrect(incorrectScore)
                Game()
            }
        }


    }
}